#! /bin/bash -e

# pull images for local build
aws s3 sync --size-only --exclude "*.DS_Store*" --exclude "*.sh" s3://theolers-family-static-images static/images

# build hugo site
rm -fr public
hugo

# deploy hugo site
aws s3 sync --acl public-read --size-only --delete --exclude "*static/images*" public s3://www.theolers.family
aws s3 sync --acl public-read --size-only --delete --exclude "*.sh" s3://theolers-family-static-images s3://www.theolers.family/static/images
