#!/bin/bash -e
tag=$(date +'%Y%m%d.%H%M')
docker build docker -t zwing99/theolersbuild:$tag \
                    -t zwing99/theolersbuild:latest

docker push zwing99/theolersbuild:$tag
docker push zwing99/theolersbuild:latest

