---
title: "Oler Boy's Ski Trip 2019"
date: 2019-01-19T21:08:27-06:00
draft: false
image: "/images/2019/01/olerboysskitrip-2019.jpg"
description: "The first of ski trip for J!"
---
My dad, my son and I all got a chance to go skiing in Colorado this year. The
trip coincided with J's 6th birthday. And on his birthday, it was amazing; It
was his third and final day of skiing. When I went to go pick him up from ski
school he had learned how to go up in a chair lift. So, we quickly went and
got him his after-lesson lift ticket, and proceeded to take about 10 runs
together. He skied so well! He loved to just point the skis downhill and see
how fast he could go! The rest of the time, my dad and I got to ski all over
the mountain with our friend Mel while J was in ski school. Our friend Keith
did skate skiing while we hit the slopes. All in all it was a great trip. And
I am ready to get the rest of the fam on the slopes.

Some pics:
{{< gallery dir="/images/2019/01/olerboysskitrip-2019" caption-position="none" hover-effect="shrink" />}}`
