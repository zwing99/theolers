---
title: "Hello Seattle!"
date: 2019-02-01T18:00:00-06:00
draft: false
image: "/images/2019/02/seattle.jpg"
description: "Mom and Dad ran away from home"
---

Several months ago, Zac and I decided that the first of the year would be a
good time for us to take a short vacation, just the two of us, which we have
not done in a really long time. After deciding that we booked two tickets to
Seattle, Washington. There were a couple different reasons for choosing this
destination but the main one was to visit and spend time with our dear
friends Viv and Christopher. Another plus was that it was some place that I
have never been before and was on my list of places I really wanted to travel
to. So now I can officially check that off of my list and I also plan to make
a trip back there someday.

This trip probably could not have come at a better time. By the time the trip
came around the year had already gotten off to a really rough start (it is
all a very long story of which I will probably make a blog post for later but
it will take me some time to get the courage up to write about it all). We
left early on a Thursday morning and travel went pretty flawlessly. We flew
from DSM to Salt Lake City, which I think Zac and I have added that to the
list of places we want to see because the view of the mountains from the
plane was nothing less than breathtaking. We didn't have but like a 10 minute
turn around and we were on our next plane headed to Seattle. Viv picked us up
and showed us around downtown Seattle. We went and scoped out the market, got
some yummy food, fed the seagulls, got coffee at the ORIGINAL Starbucks, and
then headed to the Museum of Pop culture. It was an awesome experience. Zac
and I really enjoyed bumming around the museum. We both felt like kids!
Before we left Seattle to head to Viv's house in Olympia, we ate at a really
awesome Brazilian BBQ place with Viv and met up with Zac's friend Jason.
That was a pretty awesome experience as well. They came around with fresh
meat to your table and cut it off onto your plate!!! So yummy! We spent the
next day just hanging out, talking, and playing games. We tried out Korean
BBQ that night and met up with some good friends of Chris and Viv's. IT's
really nice to be able to go some place where we can just chill and relax and
not have to worry about what we have to do next. Saturday was a chill morning
trying out a local coffee place, more games, the Hands on Children's museum
with Chris, Viv, their daughter, and then their sister in law and nephew.
Again, we felt like little kids! It was great!!! That night Viv and Chris
treated us to a really nice fresh seafood restaurant. I was able to have my
fresh shrimp that I love and Zac had some really good salmon. We had great
conversation and then spent the night back at their house playing a new game
that Zac had gotten for the Nintendo Switch. Sunday we bummed around, got
breakfast, and then prepped to head home. We thought we may have a rough time
getting home because of the weather that would be going through Minneapolis
at the time that we would have our layover. We were able to get our flights
switched to going through Salt Lake City instead which only made us get into
DSM a half hour later. It was nice to get home and wake our groggy kids up to
tell them mommy and daddy were home.

The only bummer was that I ended up with several fun bugs when I got home, so
I have been holed up in the house all week. Probably due to all the germs
that get passed around on planes! Ick!

Shout out to both set of our parents and my sis for helping out with kiddos
so that we could go have a weekend away! Thank you all so much!

Some pics:
{{< gallery dir="/images/2019/02/seattle" caption-position="none" hover-effect="shrink" />}}`