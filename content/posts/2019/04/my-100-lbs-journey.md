---
title: "My -100lbs Journey"
date: 2019-04-19T20:00:00-05:00
draft: false
image: "/images/2019/04/my-100-lbs-journey.jpg"
description: "My crazy year of weight loss."
---

## Before
{{< figure link="/images/2019/04/my-100-lbs-journey/1.jpg" thumb="-thumb" caption-position="none" hover-effect="shrink" width="370px">}}

## The Story
In April of 2018, I hit my largest weight EVER. I was 316lbs on the scale
and felt generally uncomfortable in my skin. I took showers and
realized that i could no longer see my feet. I just felt unhealthy and winded
all the time. I would walk up the stair at work and have to catch my breath.
I could hardly take walks with my kids or play games in the yard. Something
had to change.

On the advice of a couple of friends, I decided to try the Keto Diet (Low
Carb, Moderate Protein, High Fat). The trick to the diet according to several
of them was just to try and stay under 20 net carbs per day. So I did it, I
gave up what seemed like a million of my favorite foods; bread, pasta, cake, fruit,
hostess snacks, etc. Furthermore, Becky agreed to do it with me, which was part of my ticket to
success. So, it began.

I lost close to 20lbs in the first month. And as the months kept
adding up the weight just kept coming off. I picked up tricks such as replacing
breakfast with bulletproof coffee, eating peanut butter as a desert, and
whiskey over beer. Literally like clock work the pounds just kept coming off
at a near constant rate.

Key milestones I remember about this journey: (1) Clothes shopping days. Every
few months I got to buy a new pairs of jeans because my old ones just plain
did not fit. I have gone from a size 44x32 to a size 34x32. (2) I also remember
when I had lost enough weight to equal the combined weight of all three of my
children. (3) Finally, just being able to just take walks with my kids and not
feel exhausted when i am done.

Finally after one whole year of doing this diet, almost exactly, I hit a
milestone weight of 216lbs. #micDrop. Amazing. Such a blessing, I get
comments constantly of how much I have changed and it feels great. I am not
sure when I will be done with the Keto diet but I am thankful for the results
and changes it has brought in my life.

## After
{{< figure link="/images/2019/04/my-100-lbs-journey/2.jpg" thumb="-thumb" caption-position="none" hover-effect="shrink" width="200px">}}