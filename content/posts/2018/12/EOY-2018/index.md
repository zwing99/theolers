---
title: "End Of Year 2018"
date: 2018-12-31T23:59:59-06:00
draft: false
image: "/images/2018/12/EOY-2018.jpg"
description: "Our year 2018 in review. Another year Oler."
---
# 2018
The other day Zac suggested that it might be a good idea and or time for me to make an end of the year post, summing up our year. We aren't the best at typing up and sending out Christmas letters so our thought was that blog posts might be more of our style. 
I was thinking about what would be best to sum up our year and came up with the idea to sum up month by month. There may be some details that are a little vague so feel free to message me with questions but leaving out those details in respect of others.

## January
Celebrated our dear friend Heidi's 30th birthday.
Celebrated J's fifth birthday and he had his first official birthday party with a few of his close friends
Zac's parents moved from Urbandale to Ankeny.
{{< figure link="/images/2018/12/EOY-2018/11.jpg" thumb="-thumb" caption-position="none" hover-effect="shrink" width="230px">}}


## February
This month is full of birthdays for our family and close friends that have birthdays this month as well.
Zac and I also experience a first with the passing of a very dear friend of ours.
We celebrated Gracie turning 3!
Zac and Gracie went to their second Father Daughter Valentines day dance.

{{< figure link="/images/2018/12/EOY-2018/1.jpg" thumb="-thumb" caption-position="none" hover-effect="shrink" width="300px">}}

## March
This month came in with a BANG as Zac and I became aunt and uncle for the first time to our nephew Joshua who was born to my sister KayDee and my brother in law Cory.
My dear friend Heidi and I traveled to Cameron, Missouri to meet our dear friend Vivienne and got to meet her dear daughter for the first time!
My parents remodeled their kitchen, living room, and bathroom. It is so beautiful.
{{< figure link="/images/2018/12/EOY-2018/6.jpg" thumb="-thumb" caption-position="none" hover-effect="shrink" width="380px">}}

## April
April was a very busy month that welcomed some changes.
Our dear friend Jen (my honorary little sister) got married to her husband Jake who I actually graduated from high school with.
J was a ring bearer and I had the privilege of being one of Jen's personal attendants.
That week before the wedding Zac and I started the Keto diet and have seen some really great results.
The kids and I spent a lot of time with KayDee and Joshua.
{{< figure link="/images/2018/12/EOY-2018/12.jpg" thumb="-thumb" caption-position="none" hover-effect="shrink" width="300px">}}

## May
More quality time with KayDee and Joshua.
Zac turned 32.
We participated in our church's annual Mission to the City project over memorial day weekend.
Our dear friends Brandon and Brigette came from Illinois and spent a couple of days with us. So thankful for the time that we got to spend with them!
{{< figure link="/images/2018/12/EOY-2018/7.jpg" thumb="-thumb" caption-position="none" hover-effect="shrink" width="380px">}}

## June
I turned 33
Zac and I traveled to Racine at the end of the month for Zac's cousin Zoe's wedding reception for her and her husband Tom.
During that time we also got to spend time with Zac's sister Jennie who lives with her husband Jake out in Basalt, Colorado.
{{< figure link="/images/2018/12/EOY-2018/2.jpg" thumb="-thumb" caption-position="none" hover-effect="shrink" width="380px">}}

## July
We spent a week in Racine for the 4th of July. We love the parade and we also love spending time with Zac's grandma Mary and many other of Zac's family members.
Zac and I attended Summer Fest in Milwaukee and saw The Bay Ledges and The Fray which is one of our all time favorite bands. Zac's parents were very kind in watching the kiddos so that we could go! 
Iz turned 1!!!!!
I started watching Joshua 4 days a week .
{{< figure link="/images/2018/12/EOY-2018/8.jpg" thumb="-thumb" caption-position="none" hover-effect="shrink" width="230px">}}

## August
We took our first trip as a family out to Steamboat Springs, Colorado with Zac's parents. It was so much fun. We are so thankful for the time we spent out there this year and are very excited for future trips out there.
Our dear friends Matt and Latrice got married. I had the honor of being one of Latrice's personal attendants.
My parents celebrated their 36th wedding anniversary.
{{< figure link="/images/2018/12/EOY-2018/9.jpg" thumb="-thumb" caption-position="none" hover-effect="shrink" width="230px">}}

## September
Zac's parents celebrated their 35th wedding anniversary .
I started home school with J (How is he in Kindergarten already?)
My parents took a trip to Arkansas so the kids and I took care of their cat Smokey by going over to the house to check on him .
We helped friends out by going over and checking on and spending time with their Ferret.
{{< figure link="/images/2018/12/EOY-2018/10.jpg" thumb="-thumb" caption-position="none" hover-effect="shrink" width="230px">}}

## October
Zac and I celebrated our 8 year anniversary.
We got to go on a Home Instruction Field Trip to a Wildlife Prairie.
Halloween was lots of fun!!!
{{< gallery dir="/images/2018/12/EOY-2018/october" caption-position="none" hover-effect="shrink" />}}`

## November
Zac and I hosted thanksgiving at our house. It was such a blessing.
My sister became a stay at home mom so now the kids and I get to spend time with the both of them a day or two every week.
Zac took a trip to the conference QCON in San Fran and got to see and walk across the Golden Gate Bridge.
{{< figure link="/images/2018/12/EOY-2018/4.jpg" thumb="-thumb" caption-position="none" hover-effect="shrink" width="240px">}}


## December
Lot and lots of shopping, wrapping up the school semester and lots of time with family and friends.
We got to spend some time with Zac's sister Jennie for a little bit and we all were so excited! The kids were super pumped so get to spend some quality time with her!
{{< gallery dir="/images/2018/12/EOY-2018/december" caption-position="none" hover-effect="shrink" />}}`

# A few other things to note
J played soccer this summer in our church league on a team that Zac and my dad coached together.
J and G started AWANA clubs in September (J is a Spark and G is a Cubbie).
Zac helps with J's Spark group and I help out with G's Cubbie's group while Iz gets some time alone with her grandma and grandpa Cross which is hard time get when you are the third child so she soaks it up.
We spent a lot of time with both sets of parents which is such a blessing to us. We love seeing how much our kids love all of them and how they love our kids.
The kids have also been spoiled by their aunt KayDee and Uncle Cory with surprise trips to the zoo, talks of fishing trips, playing outside with uncle and legos.
I am sure that I left something out but I thinks this pretty much sums up our year!
Happy New Year everyone!!!
{{< figure link="/images/2018/12/EOY-2018/3.jpg" thumb="-thumb" caption-position="none" hover-effect="shrink" width="400px">}}

Love, Becky and Zac (and the rest of the clan too!!)
