---
title: "Colorado 2018"
date: 2018-08-30T12:00:00-06:00
image: "/images/2018/08/colorado-2018.jpg"
description: "Our first big road trips as a whole family. Such a beautiful place."
draft: false
---
This past summer we endeavored a trip out to Steamboat Springs, Colorado where my
parents own a fantastic condo. I had often gone to Colorado as a child for
skiing but I had not ever been there during the summer. The sites blew us all
away. It was so amazing taking hikes with the kids and seeing the beauty of
God's creation in the mountains and lakes and valleys. We will be going back
soon, lil' J and I are already planning a ski trip in January for his birthday.

Some pics:
{{< gallery dir="/images/2018/08/colorado-2018" caption-position="none" hover-effect="shrink" />}}`
