---
title: Be my valentine 
type: post
date: 2015-04-08T22:37:41+00:00
image: "/images/oldblog/be-my-valentine/img_0439.jpg"
---
So a lot has happened since my last post. I feel like just about as much stuff has happened in two months as it did all last year.

Around February 2nd I started having some pretty strong contractions. They were strong enough that I went to the hospital once to get checked out and then had a non stress test in the office. Both times they sent me home to take some sleep aids to get the contractions to stop since I was not due until the 25th.

On Friday February 13th I attended my great Aunt's funeral with my parents and sister. Afterwards my sis and I went for lunch at the mall then walked a little bit. Later that evening I started having some slight contractions so I told Zac "let's go to the mall and walk". We spent about an hour walking around. By 930 I called my sis to have her come sit with J because the midwives suggested I come in to the hospital to get checked out. I had not progressed at all by midnight so they sent me home and told me to take a sleep aide like they did before. I went home and by 5am I had not slept and the contractions hadn't stopped. They told me to come back in so I called mom so that Zac could get some rest especially if I wasn't going in to labor. I went in right at shift change so I got a new nurse and my midwife had changed over as well. The midwife who came on was the same one who delivered J so that was a comfort to me just because she knows how crazy my body is with labor. They finally decided around 8am to give me a shot to either stop the contractions or send me in to labor fully. They rooms were almost full so they were talking about keeping me on the gurney in triage but my nurse had a strong feeling that I was going to have the baby that day so she made sure I got in to a room. After 3 hours there was no change in my contractions so the nurse got my midwife, they checked me and I changed enough that they broke my water. I was so tired from being up for 36 hours that I chose to have an epidural. I started changing really fast so by the time Zac arrived I was almost ready to deliver. By 2:33 little miss G had arrived. Our little valentine! She had to be checked out by the NICU because there was a chance she had swallowed some maconium. She had to have some oxygen for just a couple minutes. But after that she pinked up fast and was fine. Healthy baby girl! 6lbs 10oz and 19inches long.

Big brother loves his sister! He met her that day and was immediately a protector and wanted to hold her the whole time. We are very happy to have G in our lives. She is a true joy.

About a 3 weeks after G was born I started getting pain in the upper quadrant of my stomach on the right side. I just thought it was normal pains from having things shifted around during Burt but after a few days Zac convinced me to go to the doctor. To make a long story short after visiting the doctor, having an ultrasound and visiting a surgeon we found out that I had a faulty gallbladder and a small hernia. So I spent St. Patrick's day having surgery and recovering. So the first month of G's life was an interesting one.

Here are some pictures of the first few days of G's life and a few recent ones.

{{< gallery dir="/images/oldblog/be-my-valentine" caption-position="none" hover-effect="shrink" />}}