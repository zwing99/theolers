---
title: Four months!
type: post
date: 2013-06-04T13:54:21+00:00
image: "images/oldblog/4-months/3.jpg"
---
So I missed the 3 month post but thought I could make it up in this post.
  
Things have been going well here. The church plant on the south side of Des Moines that we are a part of is thriving and it had been really great to get to know the people we are serving with better and also getting to know newcomers. May has been a pretty eventful month so far. We celebrated my grandpa Cecil's 82nd birthday May 12, the day after aunt Jennie's birthday (Zac's sister). I celebrated my first Mothe's Day which was great. Zac and J bought me a beautiful bouquet of flowers and Zac made a crazy awesome card online and had it sent to our mailbox. We were able to spend some time with my mom during the day and then was able to see Zac's mom in the evening time.
  
We've been spending more time with both sets of parents lately and it has been a great blessing to get to watch them interact with J and see how much joy he brings to them. Zac's birthday was Monday May 13 and we spent it with his parents and aunt Amy, aunt Mary Lou and uncle Nik. It was also when J turned 4 months old. I think Zac was more excited about that than his own birthday. We had pizza and birthday cake and just hung out and talked. It was a good evening. 

J had his check up and shots May 15 His little body did not like this set very much but he's such a happy baby that that amount of fussy was slightly abnormal 

{{< gallery dir="/images/oldblog/4-months" caption-position="none" hover-effect="shrink" />}}`