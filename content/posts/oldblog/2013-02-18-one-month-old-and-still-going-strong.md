---
title: One Month Old and Still Going Strong
type: post
date: 2013-02-18T23:00:28+00:00
image: "/images/oldblog/one-month-and-still-going-strong/1.jpg"
---
We have a little peanut for a son but he is sure growing. He had his one month appointment last week and he has grown 2 and 1/4 inches and has gained a pound and a half from his lowest weight which he was 6.3 pounds the wednesday after he was born. He is now up to 7lbs 10oz and still growing strong. He has been a busy boy this last week and a half. Since he is a little bit older we have been able to take him more places and he has been able to meet more people.
  
So this last week he was able to meet all of his great grandparents. We were able to physically go to Arkansas and see great grandma and grandpa Carbarnes but praise The Lord for computers and for Skype! We Skyped with them a week ago this last Friday.

Then a week ago Saturday we took J to meet his great Grandpa Cecil which was a blessing because 3 days after J got to meet him we had to put Grandpa in a memory care unit. He all of a sudden came down with a very progressive form of Dementia called Vascular Dementia. He is still in really good physical health he just has a hard time remembering things and he has some delusions especially in the evening time. But the place where we have him is where my mom and I both work. If you have any questions or want to know where he is so you can visit or send him mail just either comment or send me an email or facebook me.

Yesterday J was able to meet his Great Grandma Maxine for the first time. Grandma had had the stomach flu and it was also going around the place where she lives so we all decided that it was best to wait to take him to see her so as not to risk him getting sick. There is a gal who I know who takes care of grandma that has been able to show her pictures and videos of our little man so that she was not totally out of the loop. She was very excited to see him yesterday. She would not take her eyes off him. She has a mild form of Alzheime's disease, but still remembers people and for sure remembered that he was born and that she needed to spoil him with hugs and kisses.

A year and a half ago we lost a very dear friend of ours from church Wanda Sears who was like a grandma to my sister and I. When Rasheila was going through Wanda's many scraps of fabric and quilt squares she found these turtle squares that were made out of cute fabric that has animals that were taking xrays. Rasheila thought of me and my medical background right away and gave them to me at one of my baby showers. We gave it to my mom and she was able to make a floor mat like blanket for JR. Today he spent some time on it while mommy was picking up the living room. We had a fun day! I really enjoy being his mommy and getting to spend my days with him.

{{< gallery dir="/images/oldblog/one-month-and-still-going-strong" caption-position="none" hover-effect="shrink" />}}`