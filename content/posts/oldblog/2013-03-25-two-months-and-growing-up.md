---
title: Two Months and growing up
type: post
date: 2013-03-25T15:45:07+00:00
image: "/images/oldblog/two-months-and-growing-up/1.jpg"
---
So it has been a while since I last blogged and a lot has gone on since the last post.

As most of you may know Zac and I were asked at the beginning of this last fall to go with about 50 other people from our Downtown Church location and a few from the Windsor Heights location to start a new church plant on the South Side of Des Moines. Since then we have finally gotten our occupancy so that meant getting the building ready to start having church in the new building. Zac has helped out a lot with the sound system at the Windsor Heights location so he has been a great asset to the South Side in getting the sound system up and running, so he has been working very hard at getting everything ready to go. We have been in the new building for 3 weeks and things are going great. There are still many things to be done in the new building but we have been able to meet there safely and it has been great. Zac was asked at the end of the year to lead worship and he seems to really enjoy it.

Our Little J is now 10 weeks old. We had his 2 month appointment and he was 9lbs 11 oz and 23 inches long. The doctor said that he was lean and mean. It has been an amazing adventure being new parents and just loving our little boy in genera
  
The last month has been busy and has been a little bit hard with certain changes that have been going on amongst family members. Growing up my sister and I spent a lot of time with our great aunts and uncles. This past month my Grandpa Chuck's sister in law was told that her cancer was back and she just passed away a week and a half ago but not before we were able to get J up to see her. It was a joyous day to see how happy it made her to see her great great nephew. This past Saturday my Grandpa Chuck's last living sister passed away. So it has been a month of funerals and grief but joy as well in knowing that The Lord has given peace and healing to his servants. The picture with the four ladies the only one left in my grandmother (the lady on the left) My aunt Virginia (the lady second from the right) just passed away this past Saturday and aunt Lois ( the end, far right) passed this last fall. Aunt Jeri is the one right next to my grandmother. I am super blessed to have known these ladies.
  
My cousin John was able to meet J for the first time during his spring break. As you can probably tell by the picture he was pretty excited to meet him.
  
J is growing like a weed. It has been so great to introduce him to family members and friends that have not met him yet. Grandma Ellen and Grandpa John had been gone for two and three weeks to Colorado to ski and spend time at their condo in Steam Boat. When they got back it was great to be able to spend the day with them and to see how they were amazed at how much Little J had grown up. My parents are also enjoying watching him grow up. It is also amazing to see how J responds to the 4 of them. He knows who his grandparents are.
  
My sister has been able to spend some quality time with him and she has told me several times that she is actually excited for Jennie and Jake to come to town so that she can share him with them. She loves him a lot and is excited for Jennie and Jake to meet him.

{{< gallery dir="/images/oldblog/two-months-and-growing-up" caption-position="none" hover-effect="shrink" />}}`