#! /bin/bash -e
for i in *.jpg
do 
    convert -resize x250 $i ${i/.jpg/}-thumb.jpg
done
