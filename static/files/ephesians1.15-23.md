# Ephesians 1:15-23
## Review of promises:
1. Predestined us to be adopted as children of God.
2. Redemption by the blood of Christ spilled on the cross. COMPLETE FORGIVENESS
3. Inheritance in heaven along with the Jews
4. The promised seal of the Holy Spirit.

**Observation before we begin, Paul is praying for and about the Ephesians. I think we can use this as a rubric for our prayers about ourselves and our fellow believer and for our community group**
- - - -
## Passage
> [15]This is why, since I heard about your faith in the Lord Jesus and your love for all the saints, [16]::I never stop giving thanks for you:: as I remember you in my prayers. [17]I pray that the God of our Lord Jesus Christ, the glorious Father, would give you ::the Spirit of wisdom and revelation in the knowledge of him.:: [18]I pray that the ::eyes of your heart may be enlightened:: so that you may know what is the ::hope of his calling::, what is the wealth of his glorious inheritance in the saints, [19]and what is the ::immeasurable greatness of his power toward us:: who believe, according to the mighty working of his strength.  
> [20]He exercised this power in Christ by raising him from the dead and seating him at his right hand in the heavens— [21]far above every ruler and authority, power and dominion, and every title given, not only in this age but also in the one to come.[22]And he subjected everything under his feet and appointed him as head over everything for the church, [23]which is his body, the fullness of the one who fills all things in every way.  

## v15-16: Giving Thanks and Praying for our brothers and sisters.
* What practically can we learn from v15-16?
* How will it affect our brothers and sisters if we give thanks and pray for them?
* How will it affect us?

> 1 John 3:16 - This is how we have we have come to know love: He laid down his life for us. We should also lay down our lives for our brothers and sisters.  

> John3:27-30 - John responded, "No one can receive anything unless it has been given to him from heaven. You yourselves can testify that I said, ‘I am not the Messiah, but I’ve been sent ahead of him.’ He who has the bride is the groom. But the groom’s friend, who stands by and listens for him, rejoices greatly at the groom’s voice. So this joy of mine is complete. ::He must increase, but I must decrease."::  

## v17: God’s spirit
* What promises do we have about "The Spirt of wisdom and revelation in the knowledge of him"  (turn to neighbor)
	* Counselor/Helper John 16:7 - Nevertheless, I am telling you the truth. It is for your benefit that I go away, because if I don’t go away the ::Counselor:: will not come to you. If I go, I will send him to you.
	* Power/Strength - 2 Timothy 1:7 - For God has not given us a spirit of fear, but one of ::power, love, and sound judgment.::
	* Prayer Partner - Romans 8:26-27 - In the same way the Spirit also helps us in our weakness, because we do not know what to pray for as we should, but the Spirit himself intercedes for us with unspoken groanings. And he who searches our hearts knows the mind of the Spirit, because ::he intercedes:: for the saints according to the will of God.
	* Fruitful Life - Galatians 5:22-23 - But the fruit of the Spirit is love, joy, peace, patience, kindness, goodness, faithfulness, gentleness, and self-control. The law is not against such things.

## v18: Hope of his Calling
* What does Paul mean when he says the "eyes of our heart"?
	* Oswald Chambers - [Is Your Ability to See God Blinded? | My Utmost For His Highest](https://utmost.org/is-your-ability-to-see-god-blinded/)
* What is the "hope" of his calling
	* Romans 5:6-11 - [6] For while we were still helpless, at the right time, Christ died for the ungodly. [7]For rarely will someone die for a just person—though for a good person perhaps someone might even dare to die. [8[But God proves his own love for us in that while we were still sinners, Christ died for us. [9]How much more then, since we have now been declared righteous by his blood, will we be saved through him from wrath. [10]For if, while we were enemies, we were reconciled to God through the death of his Son, then how much more, having been reconciled, will we be saved by his life. [11]And not only that, but we also rejoice in God through our Lord Jesus Christ, through whom we have now received this reconciliation.
	* Romans 8 - 32 He did not even spare his own Son but offered him up for us all. a How will he not also with him grant us everything? 33 Who can bring an accusation against God’s elect? b God is the one who justifies. 34 Who is the one who condemns? Christ Jesus is the one who died, but even more, has been raised; he also is at the right hand of God and intercedes for us. 35 Who can separate us from the love of Christ? Can affliction or distress or persecution or famine or nakedness or danger or sword? 36 As it is written: Because of you we are being put to death all day long; we are counted as sheep to be slaughtered. 37 No, in all these things we are more than conquerors through him who loved us. 38 For I am persuaded that neither death nor life, nor angels nor rulers, nor things present nor things to come, nor powers, 39 nor height nor depth, nor any other created thing will be able to separate us from the love of God that is in Christ Jesus our Lord.

## v19-23: Immeasurable Greatness of His Power
* How powerful is God?
	* What verses of promise do we have?
	* Psalm 18
* 1 Peter 1:3-5 - [3]Blessed be the God and Father of our Lord Jesus Christ. Because of his great mercy he has given us new birth into a living hope through the resurrection of Jesus Christ from the dead [4]and into an inheritance that is imperishable, undefiled, and unfading, kept in heaven for you. [5]You are being guarded by God’s power through faith for a salvation that is ready to be revealed in the last time

## What practical can I do different with my life because of truth from Ephesians 1?
- What should i be praying about for myself and for others?
- What is one thing i can pray for this week for someone else here?
